call plug#begin()
	Plug 'prabirshrestha/async.vim'
	Plug 'prabirshrestha/vim-lsp'
	Plug 'prabirshrestha/asyncomplete.vim'
	Plug 'prabirshrestha/asyncomplete-lsp.vim'
	Plug 'mattn/vim-lsp-settings'
	Plug 'mattn/vim-sonictemplate'
	Plug 'mattn/emmet-vim'
	Plug 'mattn/vim-maketable'
	Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' }
call plug#end()

source ~/.vim/plugconfig/lsp/main.vim
