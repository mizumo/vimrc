.PHONY: all

all: vimrc plugged

vimrc: vimrc.default
	cp ./vimrc.default ./vimrc

plugged: vimrc autoload/plug.vim plug.vim
	vim -c ":PlugUpdate" -c ":qa"
	touch plugged

autoload/plug.vim:
	curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
