set foldmethod=expr
	\ foldexpr=lsp#ui#vim#folding#foldexpr()
	\ foldtext=lsp#ui#vim#folding#foldtext()
