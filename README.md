# vimrc

本プロジェクトは、vimの初期構築を容易にするために作った`.vim`のテンプレートである。

## Set up

特にこだわりがなければ、`~/.vim` にクローンすればよい。

```bash
git clone https://gitlab.com/mizumo/vimrc.git ~/.vim
```
