nnoremap <F3> :set hlsearch!<CR>
nnoremap <F4> :set cursorline!<CR>:set cursorcolumn!<CR>
nnoremap <C-o> :copen<CR>
nnoremap gp `[v`]

source ~/.vim/plugconfig/lsp/map.vim
